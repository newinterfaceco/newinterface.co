 <!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Ajax portfolio template">
    <meta name="author" content="layerz">

    <title>Kovan — Ajax Portfolio Template</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700|Unna:400,400i,700">

    <link rel="icon" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/reset.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/main.css">

</head>
<body>

<!-- header -->
<header id="header">
    <div class="sidebar">
        <div class="loader"></div>

        <div class="trigger">
            <div class="icon-bar top"></div>
            <div class="icon-bar middle"></div>
            <div class="icon-bar bottom"></div>
        </div>

        <div class="logo">
            <a href="/" class="ajax-link">
                <img src="/logo.png">
            </a>
        </div>
    </div>

    <nav class="site-nav">
        <div class="nav__content">
            <p class="smaller">Menu</p>

            <ul class="menu">
                <li>
                    <a href="#">Portfolio</a>
                    <ul class="sub-menu">
                        <li><a href="index.html" class="ajax-link">Two Halfs</a></li>
                        <li><a href="portfolio-three-halfs.html" class="ajax-link">Three Halfs</a></li>
                        <li><a href="portfolio-four-halfs.html" class="ajax-link">Four Halfs</a></li>
                        <li><a href="portfolio-featured-halfs.html" class="ajax-link">Featured Halfs</a></li>
                    </ul>
                </li>
                <li><a href="about.html" class="ajax-link">About</a></li>
                <li>
                    <a href="#" class="ajax-link">Journal</a>
                    <ul class="sub-menu">
                        <li><a href="journal.html" class="ajax-link">Posts List</a></li>
                        <li><a href="single-post.html" class="ajax-link">Single Post</a></li>
                    </ul>
                </li>
                <li><a href="contact.html" class="ajax-link">Contact</a></li>
                <li><a href="https://themeforest.net/cart/add_items?ref=layerz&amp;item_ids=19642845">Buy!</a></li>
            </ul>

            <p class="copy smaller">2017 &copy; Kovan.</p>
        </div>
    </nav>
</header>


<!-- wrapper -->
<div id="wrapper">
    <div id="inner">

        <ul class="filters">
            <li class="selected">
                <span>All</span>
                <ul>
                    <li><a data-filter="*" class="active">All</a></li>
                    {{--<li><a data-filter=".art">Art Direction</a></li>--}}
                    <li><a data-filter=".website">Website's</a></li>
                    {{--<li><a data-filter=".photography">Photography</a></li>--}}
                    <li><a data-filter=".graphic">Logo's</a></li>
                </ul>
            </li>
        </ul>

        <div class="works">
            <div class="gallery" data-cols="2" data-margin="0" data-height="1">

                <div class="entry work-entry">
                    <a href="single-project.html" class="ajax-link">
                        <div class="entry__image" data-bg="/newinterface-big-logo.png"></div>
                        <div class="overlay hidden" data-overlay="9">
                            <div class="overlay-content bottom">
                                <span class="project-subtitle smaller">About Us</span>
                                <h4 class="project-title">New Interface</h4>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="entry work-entry">
                    <a href="single-project.html" class="ajax-link">
                        <div class="entry__image" data-bg="/office.jpg"></div>
                        <div class="overlay hidden" data-overlay="9">
                            <div class="overlay-content bottom">
                                <span class="project-subtitle smaller">The Office</span>
                                <h4 class="project-title">New Interface</h4>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="entry work-entry website">
                    <a href="single-project-2.html" class="ajax-link">
                        <div class="entry__image" data-bg="/yeoman.png"></div>
                        <div class="overlay hidden" data-overlay="9">
                            <div class="overlay-content bottom">
                                <span class="project-subtitle smaller">Website</span>
                                <h4 class="project-title">Yeoman Landscaping</h4>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="entry work-entry website">
                    <a href="single-project.html" class="ajax-link">
                        <div class="entry__image" data-bg="/thevillagevegan.png"></div>
                        <div class="overlay hidden" data-overlay="9">
                            <div class="overlay-content bottom">
                                <span class="project-subtitle smaller">Website</span>
                                <h4 class="project-title">The Village Vegan</h4>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="entry work-entry logo">
                    <a href="single-project-3.html" class="ajax-link">
                        <div class="entry__image" data-bg="/wilsonlandscaping.png"></div>
                        <div class="overlay hidden" data-overlay="9">
                            <div class="overlay-content bottom">
                                <span class="project-subtitle smaller">Logo</span>
                                <h4 class="project-title">Wilson Landscaping</h4>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="entry work-entry logo">
                    <a href="single-project.html" class="ajax-link">
                        <div class="entry__image" data-bg="/oscepassmentors.png"></div>
                        <div class="overlay hidden" data-overlay="9">
                            <div class="overlay-content bottom">
                                <span class="project-subtitle smaller">Logo</span>
                                <h4 class="project-title">OSCE Pass Mentors</h4>
                            </div>
                        </div>
                    </a>
                </div>

                {{--<div class="entry work-entry photography">--}}
                    {{--<a href="single-project.html" class="ajax-link">--}}
                        {{--<div class="entry__image" data-bg="assets/img/works/work-06.jpg"></div>--}}
                        {{--<div class="overlay hidden" data-overlay="9">--}}
                            {{--<div class="overlay-content bottom">--}}
                                {{--<span class="project-subtitle smaller">Photography</span>--}}
                                {{--<h4 class="project-title">Shanghai Tori</h4>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</div>--}}

                {{--<div class="entry work-entry art graphic">--}}
                    {{--<a href="single-project.html" class="ajax-link">--}}
                        {{--<div class="entry__image" data-bg="assets/img/works/work-07.jpg"></div>--}}
                        {{--<div class="overlay hidden" data-overlay="9">--}}
                            {{--<div class="overlay-content bottom">--}}
                                {{--<span class="project-subtitle smaller">Art direction</span>--}}
                                {{--<h4 class="project-title">Sunglasses</h4>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</div>--}}

                {{--<div class="entry work-entry graphic">--}}
                    {{--<a href="single-project.html" class="ajax-link">--}}
                        {{--<div class="entry__image" data-bg="assets/img/works/work-08.jpg"></div>--}}
                        {{--<div class="overlay hidden" data-overlay="9">--}}
                            {{--<div class="overlay-content bottom">--}}
                                {{--<span class="project-subtitle smaller">Graphic design</span>--}}
                                {{--<h4 class="project-title">Double Exposure</h4>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</div>--}}
            </div>
        </div>

    </div>
</div>


<!-- js -->
<script src="assets/js/jquery-3.1.0.min.js"></script>
<script src="assets/js/bundle.js"></script>
<script src="assets/js/kovan.js"></script>

</body>
</html>

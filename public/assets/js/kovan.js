var ww = window.innerWidth,
	wh = window.innerHeight;

$(document).ready(function() {

	"use strict";

	ajax_link();
	image_bg();
	hero_heights();
	sliders();

});

$(window).on('load', function() {
	play_loader();
});

$(window).on('resize', function() {
	ww = window.innerWidth;
	wh = window.innerHeight;

	hero_heights();
});



/** Play loader
================================================== **/

function play_loader() {
	$('body').waitForImages({
		finished: function() {
			$('#header').find('.loader').fadeOut('slow', function() {
				$('#header').addClass('loaded');
			});
		},
		waitForAll: true
	});
}


/** Ajax
================================================== **/

function ajax_link() {
	var page;

	$('body').on('click', '.ajax-link', function() {
		page = $(this).attr('href');

		ajax(page);

		return false;
	});

	$(window).on('popstate', function() {
		page = location.href;

		ajax(page);
	});

	function ajax(page) {
		$('#header').find('.loader').fadeIn('slow');
		$('#header').removeClass('loaded open');

		toggle_nav(false);

		setTimeout(function() {
			$('#wrapper').load(page + ' #wrapper #inner', function(data) {
				var page_title = data.match(/<title>(.*?)<\/title>/);

				$(window).trigger('load');

				image_bg();
				hero_heights();
				sliders();

				document.title = page_title[1];

				if (window.location != page)
					window.history.pushState({path: page}, "", page);

				$('html, body').animate({
					scrollTop: 0
				}, 100);
			});
		}, 1000);
	}
}


/** Open/close nav
================================================== **/

$('.trigger').on('click', function() {
	if (!$('.site-nav').hasClass('visible')) {
		toggle_nav(true);
	} else {
		toggle_nav(false);
	}
});

$('#wrapper').on('click', function() {
	toggle_nav(false);
});

function toggle_nav(bool) {
	if (bool == true) {
		$('#header').addClass('open');
		$('#wrapper').addClass('fade');
		$('body').addClass('no-scroll');
		setTimeout(function() {
			$('.site-nav').addClass('visible');
		}, 700);
	}

	if (bool == false) {
		$('.site-nav').removeClass('visible');
		setTimeout(function() {
			$('#header').removeClass('open');
			$('#wrapper').removeClass('fade');
			$('.menu li').find('ul').slideUp(300);
		}, 300);
		setTimeout(function() {
			$('body').removeClass('no-scroll');
		}, 700);
	}
}

$('.menu li:has(ul)').find('a').on('click', function() {
	var parent = $(this).parent(),
		submenu = $(this).next('ul');

	if (submenu.is(':visible')) {
		parent.find('ul').slideUp(300);
	}

	if (submenu.is(':hidden')) {
		parent.siblings().find('ul').slideUp(300);
		submenu.slideDown(300);
	}

	if (parent.children('ul').length == 0) {
		return true;
	} else {
		return false;
	}
});


/** Background images
================================================== **/

function image_bg() {
	$('[data-bg]').each(function() {
		var bg = $(this).data('bg');

		$(this).css({
			'background-image': 'url(' + bg + ')',
			'background-size': 'cover',
			'background-position': '50% 50%',
		});
	});
}


/** Hero heights
================================================== **/

function hero_heights() {
	if (ww > 600) {
		if ($('.hero').hasClass('small')) {
			var height = wh * 0.7;
			$('.hero').css('height', height + 'px');
		} else if ($('.hero').hasClass('medium')) {
			var height = wh * 0.8;
			$('.hero').css('height', height + 'px');
		} else {
			$('.hero').css('height', wh + 'px');
		}
	} else {
		$('.hero').css('height', (wh / 2) + 'px');
	}
}


/** Sliders
================================================== **/

function sliders() {

	$('.project-slider').owlCarousel({
		loop: true,
		items: 1,
		nav: true,
		navText: [ '', '' ]
	});

}
